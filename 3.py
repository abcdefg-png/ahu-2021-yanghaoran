from flask import Flask
from flask import render_template
import requests

app = Flask(__name__)


@app.route('/')
def hello_world():
    return "hello world Flask"


@app.route('/plus', methods=['GET', 'POST'])
def plus():
    a = 1
    b = 1
    return str(a+b)


@app.route('/minus')
def minus():
    return "1"


@app.route('/multiply')
def multiply():
    return "9"


@app.route('/division')
def division():
    return "1.0"


if __name__ == '__main__':
    app.run()

